#!/usr/bin/env bash

### Run project=ICON: i.e., run previously-built executable, processing data into a form suitable to provide initial conditions for a CMAQ/CCTM run.
### For details see https://bitbucket.org/tlroche/cctm-build/raw/HEAD/README.md

### Very nearly exactly = (BCON-build/run_BCON.sh ~= s/bc/ic/gi). TODO: refactor!

### Expects to be run on a linux (tested with RHEL5). Requires
### * build-common code in ICON_utilities.sh
### * not-too-up-to-date `bash` (tested with version=3.2.25!)
### * `basename`
### * `date`
### * `dirname`
### * `find`
### * `head`
### * `ls`
### * `pwd`
### * `readlink`
### * `tail`
### * `tee`
### * `time`

# ----------------------------------------------------------------------
# constants
# ----------------------------------------------------------------------

### Get these out of the way before we get to work:
## message-related
THIS="$0"
# for commandline testing, give path
# THIS='/project/inf35w/roche/CMAQ-5.0.1/git/old/ICON-build/run_ICON.sh'
# Get absolute path: otherwise commands in different dir/folders write to different logs.
THIS_DIR="$(readlink -f $(dirname ${THIS}))"
THIS_FN="$(basename ${THIS})"
MESSAGE_PREFIX="${THIS_FN}:"
ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

## logging-related
DATETIME_FORMAT='%Y%m%d_%H%M'
DATETIME_STAMP="$(date +${DATETIME_FORMAT})"
LOG_FN="${THIS_FN}_${DATETIME_STAMP}.log"
# log is local to this script, not build space
LOG_FP="${THIS_DIR}/${LOG_FN}"

### more constants in function=setup* below 

### common code for build and run
UTILS="${THIS_DIR}/ICON_utilities.sh"

# ----------------------------------------------------------------------
# code
# ----------------------------------------------------------------------

# ----------------------------------------------------------------------
# functions
# ----------------------------------------------------------------------

# common code for build and run
if [[ -r "${UTILS}" ]] ; then
  echo -e "${MESSAGE_PREFIX} about to 'source' utilities file='${UTILS}'" 2>&1 | tee -a "${LOG_FP}"
  source "${UTILS}"
else
  echo -e "${ERROR_PREFIX} cannot read utilities file='${UTILS}', exiting ..." 2>&1 | tee -a "${LOG_FP}"
  exit 2
fi

function setup_ICON_run {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

  for CMD in \
    'setup_ICON_common_vars' \
    'setup_ICON_run_vars' \
    'document_ICON_run_vars' \
    'setup_ICON_run_spaces' \
    'setup_ICON_run_inputs' \
  ; do
    echo -e "\n$ ${MESSAGE_PREFIX} ${CMD}\n"
    eval "${CMD}" # comment this out for NOPing, e.g., to `source`
    if [[ $? -ne 0 ]] ; then
      echo -e "${MESSAGE_PREFIX} ${CMD}: ERROR: failed or not found\n"
      exit 7
    fi
  done
} # function setup_ICON_run


function setup_ICON_run_vars {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

  ### per run.icon:
#  ## turn off excessive WRITE3 logging
#  export IOAPI_LOG_WRITE='F'
  ## something about grids?
  export IOAPI_ISPH='19'
  ## is support needed for large timestep records (>2GB)?
  export IOAPI_OFFSET_64='NO'

  ### our main output
  if   [[ "${ModType}" == 'profile' ]] ; then
    ICON_OUTPUT_FN="${ICON_EXEC_PREFIX}_${CFG}_profile"
  elif [[ "${ModType}" == 'm3conc' ]] ; then
    RUN_DATE='2006213' # from tarball run.icon
    ICON_OUTPUT_FN="${ICON_EXEC_PREFIX}_${CFG}_${RUN_DATE}"
  else
    echo -e "${ERROR_PREFIX} cannot handle ModType='${ModType}', exiting ..." 2>&1 | tee -a "${LOG_FP}"
    exit 2
  fi

  ### where to put output
  ICON_OUTPUT_DIR_NAME="RUN_${ICON_EXEC_PREFIX}"
  ICON_OUTPUT_DIR="${M3HOME}/${ICON_OUTPUT_DIR_NAME}"
  ICON_OUTPUT_FP="${ICON_OUTPUT_DIR}/${ICON_OUTPUT_FN}"
  export INIT_CONC_1="${ICON_OUTPUT_FP} -v"

  ### grid definition: common with run_BCON.sh
  export GRID_NAME="${CFG}"
  # I expect all project repos to be peer dirs: see CMAQ-build/repo_creator.sh
  GRID_DEFN_FN='GRIDDESC'
  GRID_DEFN_DIR="$(dirname ${THIS_DIR})/CMAQ-grids/${GRID_NAME}"
  GRID_DEFN_FP="${GRID_DEFN_DIR}/${GRID_DEFN_FN}"
  export GRIDDESC="${GRID_DEFN_FP}"

  ### common with run_BCON.sh
  PROFILE_MECH="$(echo -e ${ModMech} | tr '[:lower:]' '[:upper:]')" # why do they *do* this? wouldn't it be easier to leave lowercase?
  export EXECUTION_ID="${ICON_EXEC_FN}" # not to confuse with EXEC_ID ...
  ## vertical layer definition
  export LAYER_FILE="${M3DATA}/mcip/METCRO3D_Benchmark"

  ## species definitions: common with run_BCON.sh
  SPECIES_DIR="${ICON_BUILD_DIR_PATH}"
  GC_NAMELIST_FN="GC_${Mechanism}.nml"
  AE_NAMELIST_FN="AE_${Mechanism}.nml"
  NR_NAMELIST_FN="NR_${Mechanism}.nml"
  TR_NAMELIST_FN='Species_Table_TR_0.nml'
  GC_NAMELIST_FP="${SPECIES_DIR}/${GC_NAMELIST_FN}"
  AE_NAMELIST_FP="${SPECIES_DIR}/${AE_NAMELIST_FN}"
  NR_NAMELIST_FP="${SPECIES_DIR}/${NR_NAMELIST_FN}"
  TR_NAMELIST_FP="${SPECIES_DIR}/${TR_NAMELIST_FN}"
  export gc_matrix_nml="${GC_NAMELIST_FP}"
  export ae_matrix_nml="${AE_NAMELIST_FP}"
  export nr_matrix_nml="${NR_NAMELIST_FP}"
  export tr_matrix_nml="${TR_NAMELIST_FP}"

  ### these define some kinda important data :-)
  if   [[ "${ModType}" == 'profile' ]] ; then
    IC_PROFILE_FN="ic_profile_${PROFILE_MECH}.dat"
    export IC_PROFILE="${ICON_BUILD_DIR_PATH}/${IC_PROFILE_FN}"
  elif [[ "${ModType}" == 'm3conc' ]] ; then
    export CTM_CONC_1="${M3DATA}/cctm/CCTM_d1bCONC.d1b"
    if [[ ! -r "${CTM_CONC_1}" ]] ; then
      echo -e "${ERROR_PREFIX} cannot read CTM_CONC_1='${CTM_CONC_1}', exiting ..." 2>&1 | tee -a "${LOG_FP}"
      exit 2
    fi
    # following values all from tarball run.icon
    export MET_CRO_3D_CRS=''
    export MET_CRO_3D_FIN=''
    export SDATE="${RUN_DATE}"
    export STIME='000000'
#    export RUNLEN='240000' # in run.bcon but not run.icon
  fi

} # function setup_ICON_run_vars

### document current configuration
function document_ICON_run_vars {
  echo -e "LOG_FP='${LOG_FP}'" | tee -a "${LOG_FP}"
  echo -e "ae_matrix_nml='${ae_matrix_nml}'" | tee -a "${LOG_FP}"
  echo -e "AE_NAMELIST_FP='${AE_NAMELIST_FP}'" | tee -a "${LOG_FP}"
  echo -e "APPL='${APPL}'" | tee -a "${LOG_FP}"
  echo -e "ICON_EXEC_FN='${ICON_EXEC_FN}'" | tee -a "${LOG_FP}"
  echo -e "ICON_EXEC_FP='${ICON_EXEC_FP}'" | tee -a "${LOG_FP}"
  echo -e "ICON_EXEC_PREFIX='${ICON_EXEC_PREFIX}'" | tee -a "${LOG_FP}"
  echo -e "ICON_OUTPUT_DIR='${ICON_OUTPUT_DIR}'" | tee -a "${LOG_FP}"
  echo -e "ICON_OUTPUT_FN='${ICON_OUTPUT_FN}'" | tee -a "${LOG_FP}"
  echo -e "ICON_OUTPUT_FP='${ICON_OUTPUT_FP}'" | tee -a "${LOG_FP}"
  echo -e "IC_PROFILE='${IC_PROFILE}'" | tee -a "${LOG_FP}"
  echo -e "IC_PROFILE_FN='${IC_PROFILE_FN}'" | tee -a "${LOG_FP}"
  echo -e "INIT_CONC_1='${INIT_CONC_1}'" | tee -a "${LOG_FP}"
  echo -e "ICON_BUILD_DIR_NAME='${ICON_BUILD_DIR_NAME}'" | tee -a "${LOG_FP}"
  echo -e "ICON_BUILD_DIR_PATH='${ICON_BUILD_DIR_PATH}'" | tee -a "${LOG_FP}"
  echo -e "CFG='${CFG}'" | tee -a "${LOG_FP}"
  echo -e "CMAQ_BUILD_DIR='${CMAQ_BUILD_DIR}'" | tee -a "${LOG_FP}"
  echo -e "CONFIG_CMAQ_PATH='${CONFIG_CMAQ_PATH}'" | tee -a "${LOG_FP}"
  echo -e "CSH_EXEC='${CSH_EXEC}'" | tee -a "${LOG_FP}"
  echo -e "CTM_CONC_1='${CTM_CONC_1}'" | tee -a "${LOG_FP}"
  echo -e "EXECUTION_ID='${EXECUTION_ID}'" | tee -a "${LOG_FP}"
  echo -e "gc_matrix_nml='${gc_matrix_nml}'" | tee -a "${LOG_FP}"
  echo -e "GC_NAMELIST_FP='${GC_NAMELIST_FP}'" | tee -a "${LOG_FP}"
  echo -e "GRIDDESC='${GRIDDESC}'" | tee -a "${LOG_FP}"
  echo -e "GRID_DEFN_DIR='${GRID_DEFN_DIR}'" | tee -a "${LOG_FP}"
  echo -e "GRID_DEFN_FN='${GRID_DEFN_FN}'" | tee -a "${LOG_FP}"
  echo -e "GRID_DEFN_FP='${GRID_DEFN_FP}'" | tee -a "${LOG_FP}"
  echo -e "GRID_NAME='${GRID_NAME}'" | tee -a "${LOG_FP}"
  echo -e "INPUT_ROOT='${INPUT_ROOT}'" | tee -a "${LOG_FP}"
  echo -e "IOAPI_ISPH='${IOAPI_ISPH}'" | tee -a "${LOG_FP}"
  echo -e "IOAPI_LOG_WRITE='${IOAPI_LOG_WRITE}'" | tee -a "${LOG_FP}"
  echo -e "IOAPI_OFFSET_64='${IOAPI_OFFSET_64}'" | tee -a "${LOG_FP}"
  echo -e "LAYER_FILE='${LAYER_FILE}'" | tee -a "${LOG_FP}"
  echo -e "M3DATA='${M3DATA}'" | tee -a "${LOG_FP}"
  echo -e "Mechanism='${Mechanism}'" | tee -a "${LOG_FP}"
  echo -e "MET_CRO_3D_CRS='${MET_CRO_3D_CRS}'" | tee -a "${LOG_FP}"
  echo -e "MET_CRO_3D_FIN='${MET_CRO_3D_FIN}'" | tee -a "${LOG_FP}"
  echo -e "MODEL='${MODEL}'" | tee -a "${LOG_FP}"
  echo -e "ModMech='${ModMech}'" | tee -a "${LOG_FP}"
  echo -e "ModType='${ModType}'" | tee -a "${LOG_FP}"
  echo -e "nr_matrix_nml='${nr_matrix_nml}'" | tee -a "${LOG_FP}"
  echo -e "NR_NAMELIST_FP='${NR_NAMELIST_FP}'" | tee -a "${LOG_FP}"
  echo -e "PROFILE_MECH='${PROFILE_MECH}'" | tee -a "${LOG_FP}"
  echo -e "RUNLEN='${RUNLEN}'" | tee -a "${LOG_FP}"
  echo -e "RUN_DATE='${RUN_DATE}'" | tee -a "${LOG_FP}"
  echo -e "SDATE='${SDATE}'" | tee -a "${LOG_FP}"
  echo -e "SPECIES_DIR='${SPECIES_DIR}'" | tee -a "${LOG_FP}"
  echo -e "STIME='${STIME}'" | tee -a "${LOG_FP}"
  echo -e "tr_matrix_nml='${tr_matrix_nml}'" | tee -a "${LOG_FP}"
  echo -e "TR_NAMELIST_FP='${TR_NAMELIST_FP}'" | tee -a "${LOG_FP}"
  echo -e "UBER_CONFIG_CMAQ_PATH='${UBER_CONFIG_CMAQ_PATH}'" | tee -a "${LOG_FP}"
  echo | tee -a "${LOG_FP}" # newline
} # function document_ICON_run_vars

### ensure input and output spaces
function setup_ICON_run_spaces {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

  ### inputs: don't do common with build: we want to fail on no build dir

  if   [[ -z "${ICON_BUILD_DIR_PATH}" ]] ; then
    echo -e "${ERROR_PREFIX} ICON_BUILD_DIR_PATH not defined, exiting ..." 2>&1 | tee -a "${LOG_FP}"
    exit 5
  elif [[ ! -d "${ICON_BUILD_DIR_PATH}" ]] ; then
    echo -e "${ERROR_PREFIX} build dir='${ICON_BUILD_DIR_PATH}' not found, exiting ..." 2>&1 | tee -a "${LOG_FP}"
    exit 5
  # TODO: test contents of build dir
  fi

  ### output

  if   [[ -z "${ICON_OUTPUT_DIR}" ]] ; then
    echo -e "${ERROR_PREFIX} ICON_OUTPUT_DIR not defined, exiting ..." 2>&1 | tee -a "${LOG_FP}"
    exit 5
  elif [[ -d "${ICON_OUTPUT_DIR}" ]] ; then
    echo -e "${ERROR_PREFIX} ICON output dir='${ICON_OUTPUT_DIR}' exists (move or delete it before running this script), exiting ..." 2>&1 | tee -a "${LOG_FP}"
    exit 5
  else
    for CMD in \
      "mkdir -p ${ICON_OUTPUT_DIR}" \
    ; do
      echo -e "\n$ ${MESSAGE_PREFIX} ${CMD}\n" 2>&1 | tee -a "${LOG_FP}"
      eval "${CMD}" # comment this out for NOPing, e.g., to `source`
      if [[ $? -ne 0 ]] ; then
        echo -e "${ERROR_PREFIX} ${CMD}: failed or not found\n" 2>&1 | tee -a "${LOG_FP}"
        exit 7
      fi
    done
  fi

} # function setup_ICON_run_spaces

function setup_ICON_run_inputs {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

#    'check_ICON_' \
  for CMD in \
    'check_ICON_grid' \
    'check_ICON_layers' \
    'check_ICON_species' \
  ; do
    echo -e "\n$ ${MESSAGE_PREFIX} ${CMD}\n"
    eval "${CMD}" # comment this out for NOPing, e.g., to `source`
    if [[ $? -ne 0 ]] ; then
      echo -e "${MESSAGE_PREFIX} ${CMD}: ERROR: failed or not found\n"
      exit 7
    fi
  done

  if   [[ "${ModType}" == 'profile' ]] ; then
    CMD='check_ICON_profile_inputs'
  elif [[ "${ModType}" == 'm3conc' ]] ; then
    CMD='check_ICON_conc_inputs'
  fi
  echo -e "\n$ ${MESSAGE_PREFIX} ${CMD}\n"
  eval "${CMD}" # comment this out for NOPing, e.g., to `source`
  if [[ $? -ne 0 ]] ; then
    echo -e "${MESSAGE_PREFIX} ${CMD}: ERROR: failed or not found\n"
    exit 7
  fi

} # function setup_ICON_run_inputs

function check_ICON_grid {
  if   [[ -z "${GRIDDESC}" ]] ; then
    echo -e "${ERROR_PREFIX} GRIDDESC not defined, exiting ..." 2>&1 | tee -a "${LOG_FP}"
    exit 5
  elif [[ ! -r "${GRIDDESC}" ]] ; then
    echo -e "${ERROR_PREFIX} grid definition file='${GRIDDESC}' not found, exiting ..." 2>&1 | tee -a "${LOG_FP}"
    exit 5
  # TODO: test contents (e.g., fgrep ${GRID_NAME}
  fi
} # function check_ICON_grid

function check_ICON_layers {
  if   [[ -z "${LAYER_FILE}" ]] ; then
    echo -e "${ERROR_PREFIX} LAYER_FILE not defined, exiting ..." 2>&1 | tee -a "${LOG_FP}"
    exit 5
  elif [[ ! -r "${LAYER_FILE}" ]] ; then
    echo -e "${ERROR_PREFIX} layer file='${LAYER_FILE}' not found, exiting ..." 2>&1 | tee -a "${LOG_FP}"
    exit 5
  # TODO: test contents
  fi
} # function check_ICON_layers

function check_ICON_species {

  for NAMELIST_FP in \
    "${gc_matrix_nml}" \
    "${ae_matrix_nml}" \
    "${nr_matrix_nml}" \
    "${tr_matrix_nml}" \
  ; do
    if   [[ -z "${NAMELIST_FP}" ]] ; then
      echo -e "${ERROR_PREFIX} NAMELIST_FP not defined, exiting ..." 2>&1 | tee -a "${LOG_FP}"
      exit 5
    elif [[ ! -r "${NAMELIST_FP}" ]] ; then
      echo -e "${ERROR_PREFIX} namelist file='${NAMELIST_FP}' not found, exiting ..." 2>&1 | tee -a "${LOG_FP}"
      exit 5
    # TODO: test contents
    fi
  done # for NAMELIST_FP

} # function check_ICON_species

function check_ICON_profile_inputs {
  if   [[ -z "${IC_PROFILE}" ]] ; then
    echo -e "${ERROR_PREFIX} IC_PROFILE not defined, exiting ..." 2>&1 | tee -a "${LOG_FP}"
    exit 5
  elif [[ ! -r "${IC_PROFILE}" ]] ; then
    echo -e "${ERROR_PREFIX} profile data='${IC_PROFILE}' not found, exiting ..." 2>&1 | tee -a "${LOG_FP}"
    exit 5
  # TODO: test contents
  fi
} # function check_ICON_profile_inputs

function check_ICON_conc_inputs {
  if   [[ -z "${CTM_CONC_1}" ]] ; then
    echo -e "${ERROR_PREFIX} CTM_CONC_1 not defined, exiting ..." 2>&1 | tee -a "${LOG_FP}"
    exit 5
  elif [[ ! -r "${CTM_CONC_1}" ]] ; then
    echo -e "${ERROR_PREFIX} concentration file='${CTM_CONC_1}' not found, exiting ..." 2>&1 | tee -a "${LOG_FP}"
    exit 5
  # TODO: test contents
  fi
} # function check_ICON_conc_inputs

function run_ICON {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

  if   [[ -z "${ICON_EXEC_FP}" ]] ; then
    echo -e "${ERROR_PREFIX} ICON_EXEC_FP not defined, exiting ..." 2>&1 | tee -a "${LOG_FP}"
    exit 5
  elif [[ ! -x "${ICON_EXEC_FP}" ]] ; then
    echo -e "${ERROR_PREFIX} cannot execute ICON='${ICON_EXEC_FP}', exiting ..." 2>&1 | tee -a "${LOG_FP}"
    exit 5
  fi

  for CMD in \
     "ls -alt ${ICON_OUTPUT_DIR}" \
     "time ${ICON_EXEC_FP}" \
     "ls -alt ${ICON_OUTPUT_DIR}" \
  ; do
    echo -e "\n$ ${MESSAGE_PREFIX} ${CMD}\n" 2>&1 | tee -a "${LOG_FP}"
    eval "${CMD}" # comment this out for NOPing, e.g., to `source`
    if [[ $? -ne 0 ]] ; then
      echo -e "${ERROR_PREFIX} ${CMD}: failed or not found\n" 2>&1 | tee -a "${LOG_FP}"
      exit 7
    fi
  done
} # function run_ICON

# ----------------------------------------------------------------------
# payload
# ----------------------------------------------------------------------

MESSAGE_PREFIX="${THIS_FN}::main loop: "
ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

for CMD in \
  'setup_ICON_run' \
  'run_ICON' \
; do
  echo -e "\n$ ${MESSAGE_PREFIX} ${CMD}\n"
  eval "${CMD}" # comment this out for NOPing, e.g., to `source`
  if [[ $? -ne 0 ]] ; then
    echo -e "${ERROR_PREFIX} failed or not found\n"
    exit 9
  fi
done

exit 0 # success!
