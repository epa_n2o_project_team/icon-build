# take all following from environment by default (edit as necessary)
# MODEL = ICON_V5g_Linux2_x86_64intel
# CC            = $(myCC)
# CPP           = $(FC)
# CPP_FLAGS     = 
# C_FLAGS       = -O2 -DFLDMN -I .
# f90_FLAGS     = -free -O3 -fno-alias -mp1                       -I /home/wdx/lib/x86_64i/intel/ioapi_3.1/Linux2_x86_64ifort -I . -I . -I.
# f_FLAGS       = -fixed -132 -O3 -override-limits -fno-alias -mp1 -I /home/wdx/lib/x86_64i/intel/ioapi_3.1/Linux2_x86_64ifort -I . -I . -I.
# F90_FLAGS     = -free -O3 -fno-alias -mp1                       -I /home/wdx/lib/x86_64i/intel/ioapi_3.1/Linux2_x86_64ifort -I . -I . -I.
# F_FLAGS       = -fixed -132 -O3 -override-limits -fno-alias -mp1 -I /home/wdx/lib/x86_64i/intel/ioapi_3.1/Linux2_x86_64ifort -I . -I . -I.
# FC            = $(FC)
# IOAPI         =  -L/home/wdx/lib/x86_64i/intel/ioapi_3.1/Linux2_x86_64ifort -lioapi
# LINKER        = /share/linux86_64/intel/fc/11.1.059/bin/intel64/ifort
# LINK_FLAGS    = -i-static
# MECH_INC      = /project/inf35w/roche/CMAQ-5.0.1/git/old/MECHS/cb05tucl_ae6_aq
# NETCDF        = -L/home/wdx/lib/x86_64i/intel/netcdf/lib -lnetcdf

LIBRARIES = $(IOAPI) $(NETCDF)

INCLUDES = \
  -DSUBST_GRID_ID= \
  -DSUBST_RXCMMN=\"$(MECH_INC)/RXCM.EXT\" \
  -DSUBST_RXDATA=\"$(MECH_INC)/RXDT.EXT\"

GLOBAL_MODULES = \
  UTILIO_DEFN.o \
  HGRD_DEFN.o \
  CGRID_SPCS.o \
  VGRD_DEFN.o

COMMON = \
  findex.o \
  gc_spc_map.o \
  get_envlist.o \
  icon.o \
  lat_lon.o \
  lr_interp.o \
  lst_spc_map.o \
  ngc_spc_map.o \
  opn_ic_file.o \
  setup_logdev.o \
  subhdomain.o

PROFILE = \
  prof_driver.o \
  prof_icout.o \
  prof_vinterp.o

OBJS = \
  $(GLOBAL_MODULES) \
  $(COMMON) \
  $(PROFILE)

.SUFFIXES: .F .f .c .F90 .f90

$(MODEL): $(OBJS)
	$(LINKER) $(LINK_FLAGS) $(OBJS) $(LIBRARIES) -o $@

.F.o:
	$(FC) -c $(F_FLAGS) $(CPP_FLAGS) $(INCLUDES) $<

.f.o:
	$(FC) -c $(f_FLAGS) $<

.F90.o:
	$(FC) -c $(F90_FLAGS) $(CPP_FLAGS) $(INCLUDES) $<

.f90.o:
	$(FC) -c $(f90_FLAGS) $<

.c.o:
	$(CC) -c $(C_FLAGS) $<

clean:
	rm -f $(OBJS) $(MODEL) *.mod

# dependencies

CGRID_SPCS.o:	UTILIO_DEFN.F $(MECH_INC)/RXCM.EXT $(MECH_INC)/RXDT.EXT
HGRD_DEFN.o:	UTILIO_DEFN.F
VGRD_DEFN.o:	UTILIO_DEFN.F
gc_spc_map.o:	UTILIO_DEFN.F CGRID_SPCS.F
icon.o:	HGRD_DEFN.F VGRD_DEFN.F UTILIO_DEFN.F CGRID_SPCS.F
lat_lon.o:	UTILIO_DEFN.F
lr_interp.o:	UTILIO_DEFN.F
lst_spc_map.o:	UTILIO_DEFN.F CGRID_SPCS.F
ngc_spc_map.o:	UTILIO_DEFN.F CGRID_SPCS.F
opn_ic_file.o:	HGRD_DEFN.F VGRD_DEFN.F UTILIO_DEFN.F CGRID_SPCS.F
setup_logdev.o:	UTILIO_DEFN.F
subhdomain.o:	UTILIO_DEFN.F
prof_driver.o:	UTILIO_DEFN.F CGRID_SPCS.F
prof_icout.o:	HGRD_DEFN.F VGRD_DEFN.F UTILIO_DEFN.F
prof_vinterp.o:	HGRD_DEFN.F VGRD_DEFN.F UTILIO_DEFN.F
