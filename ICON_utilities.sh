### Code common to building and running project=ICON.
### For details see https://bitbucket.org/tlroche/icon-build/raw/HEAD/README.md

### This code (err ... this whole file!) is *almost* BCON-build/BCON_utilities.sh ~= 's/bc/ic/ig'. TODO: refactor.
function setup_ICON_common_vars {
  local MESSAGE_PREFIX='ICON_utilities.sh:'
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"
  # Take whatever LOG_FP is in the environment?

  ### I expect all project repos to be peer dirs: see CMAQ-build/repo_creator.sh
  CMAQ_BUILD_DIR="$(dirname ${THIS_DIR})/CMAQ-build"
  # For discussion of config.cmaq, see
  # http://www.airqualitymodeling.org/cmaqwiki/index.php?title=CMAQ_version_5.0_%28February_2010_release%29_OGD#Configuring_your_system_for_compiling_CMAQ
  # but we depend on the bash version!
  export CONFIG_CMAQ_PATH="${CMAQ_BUILD_DIR}/config.cmaq.sh"

  ### Most important environment variables (envvars) for config.cmaq:
  ### see CMAQ-build/uber.config.cmaq.sh for details
  export UBER_CONFIG_CMAQ_PATH="${CMAQ_BUILD_DIR}/uber.config.cmaq.sh"

  ### build vars usually set in bldit.icon
  export APPL='ICON_CMAQv5_0_1' # string used in name of build dir, name of built executable
  CFG='CMAQ-BENCHMARK'
  export GRID_NAME="${CFG}"
  ICON_EXEC_PREFIX="${APPL}"    # more mnemonic

  ## These are just like BCON
  export ModType='profile'   # or 'm3conc' or 'tracer'
  export ModCommon='common'  # or ... no alternative?
  export ModMech='cb05'      # or 'saprc99' or 'saprc07t'
  export Tracer='trac0'      # or ... no alternate?
  ## chemical mechanism: choose one from MECHS/<subdir name/> ...
  export Mechanism='cb05tucl_ae6_aq'
  # ... then choose the related SuperMechanism (only used in build):
  # innovated here to kludgearound a slight difference in strings used in file paths
  SuperMechanism='cb05_ae6_aq'

  ### M3DATA not needed for build, and neither COMPILER nor INFINITY needed for run.
  ### However, one really should be running after build, if only to test.
  # TODO: test COMPILER as enum.
  # TODO: nuke the INFINITY kludge, or test as binary.
  if [[ -z "${COMPILER}" || -z "${INFINITY}" || -z "${M3DATA}" || -z "${M3LIB}" || -z "${M3HOME}" || -z "${M3MODEL}" ]] ; then
    if [[ -r "${UBER_CONFIG_CMAQ_PATH}" ]] ; then
      echo -e "${MESSAGE_PREFIX} about to 'source ${UBER_CONFIG_CMAQ_PATH}'" 2>&1 | tee -a "${LOG_FP}"
      source "${UBER_CONFIG_CMAQ_PATH}" # cannot log/tee?
    else
      echo -e "${ERROR_PREFIX} cannot find uber.config.cmaq, exiting ..." 2>&1 | tee -a "${LOG_FP}"
      exit 2
    fi
  fi

  ### but override M3MODEL for this project
  export M3MODEL="${M3HOME}/ICON" # repo with the ICON sources

  if [[ -z "${COMPILER}" || -z "${INFINITY}" || -z "${M3DATA}" || -z "${M3LIB}" || -z "${M3HOME}" || -z "${M3MODEL}" ]] ; then
    # still?
    echo -e "${ERROR_PREFIX} one of COMPILER, INFINITY, M3DATA, M3LIB, M3HOME, M3MODEL is not defined, exiting ..." 2>&1 | tee -a "${LOG_FP}"
    exit 2
  else

    if [[ ! -r "${CONFIG_CMAQ_PATH}" ]] ; then
      echo -e "${ERROR_PREFIX} cannot find '${CONFIG_CMAQ_PATH}', exiting ..." 2>&1 | tee -a "${LOG_FP}"
      exit 2
    else
      echo -e "${MESSAGE_PREFIX} about to 'source ${CONFIG_CMAQ_PATH}'" 2>&1 | tee -a "${LOG_FP}"
      source "${CONFIG_CMAQ_PATH}" # cannot log/tee?

      ## These are NOT just like BCON, so override config.cmaq*
      PARMOD='.'
      STENEX='.'
      MPI_INC='.'
      export F_FLAGS="${myFFLAGS} -I ${IOAPIMOD} -I ${PARMOD} -I ${STENEX} -I ."
      export f_FLAGS="${F_FLAGS}" # this "assignment" is only made when bldit.icon writes the Cfile :-(
      export F90_FLAGS="${myFRFLAGS} -I ${IOAPIMOD} -I ${PARMOD} -I ${STENEX} -I ."
      export CPP_FLAGS=""
      export C_FLAGS="${myCFLAGS} -DFLDMN -I ${MPI_INC}"
      export LINK_FLAGS="${myLINK_FLAG}"

      # start debugging
      case "${KEY}" in
        'CC' ) echo -e "${MESSAGE_PREFIX} CC=${CC}" 2>&1 | tee -a "${LOG_FP}" ;;
        'compiler' ) echo -e "${MESSAGE_PREFIX} compiler=${compiler}" 2>&1 | tee -a "${LOG_FP}" ;;
        'compiler_ext' ) echo -e "${MESSAGE_PREFIX} compiler_ext=${compiler_ext}" 2>&1 | tee -a "${LOG_FP}" ;;
        'CPP' ) echo -e "${MESSAGE_PREFIX} CPP=${CPP}" 2>&1 | tee -a "${LOG_FP}" ;;
        'CPP_FLAGS' ) echo -e "${MESSAGE_PREFIX} CPP_FLAGS=${CPP_FLAGS}" 2>&1 | tee -a "${LOG_FP}" ;;
        'C_FLAGS' ) echo -e "${MESSAGE_PREFIX} C_FLAGS=${C_FLAGS}" 2>&1 | tee -a "${LOG_FP}" ;;
        'EXEC_ID' ) echo -e "${MESSAGE_PREFIX} EXEC_ID=${EXEC_ID}" 2>&1 | tee -a "${LOG_FP}" ;;
        'extra_lib' ) echo -e "${MESSAGE_PREFIX} extra_lib=${extra_lib}" 2>&1 | tee -a "${LOG_FP}" ;;
        'FC' ) echo -e "${MESSAGE_PREFIX} FC=${FC}" 2>&1 | tee -a "${LOG_FP}" ;;
        'FP' ) echo -e "${MESSAGE_PREFIX} FP=${FP}" 2>&1 | tee -a "${LOG_FP}" ;;
        'F_FLAGS' ) echo -e "${MESSAGE_PREFIX} F_FLAGS=${F_FLAGS}" 2>&1 | tee -a "${LOG_FP}" ;;
        'f_FLAGS' ) echo -e "${MESSAGE_PREFIX} f_FLAGS=${f_FLAGS}" 2>&1 | tee -a "${LOG_FP}" ;;
        'f90_FLAGS' ) echo -e "${MESSAGE_PREFIX} f90_FLAGS=${f90_FLAGS}" 2>&1 | tee -a "${LOG_FP}" ;;
        'F90_FLAGS' ) echo -e "${MESSAGE_PREFIX} F90_FLAGS=${F90_FLAGS}" 2>&1 | tee -a "${LOG_FP}" ;;
        'IOAPI' ) echo -e "${MESSAGE_PREFIX} IOAPI=${IOAPI}" 2>&1 | tee -a "${LOG_FP}" ;;
        'IOAPIMOD' ) echo -e "${MESSAGE_PREFIX} IOAPIMOD=${IOAPIMOD}" 2>&1 | tee -a "${LOG_FP}" ;;
        'LINKER' ) echo -e "${MESSAGE_PREFIX} LINKER=${LINKER}" 2>&1 | tee -a "${LOG_FP}" ;;
        'LINK_FLAGS' ) echo -e "${MESSAGE_PREFIX} LINK_FLAGS=${LINK_FLAGS}" 2>&1 | tee -a "${LOG_FP}" ;;
        'M3LIB' ) echo -e "${MESSAGE_PREFIX} M3LIB=${M3LIB}" 2>&1 | tee -a "${LOG_FP}" ;;
        'mpi' ) echo -e "${MESSAGE_PREFIX} mpi=${mpi}" 2>&1 | tee -a "${LOG_FP}" ;;
        'MPI_INC' ) echo -e "${MESSAGE_PREFIX} MPI_INC=${MPI_INC}" 2>&1 | tee -a "${LOG_FP}" ;;
        'myCC' ) echo -e "${MESSAGE_PREFIX} myCC=${myCC}" 2>&1 | tee -a "${LOG_FP}" ;;
        'myCFLAGS' ) echo -e "${MESSAGE_PREFIX} myCFLAGS=${myCFLAGS}" 2>&1 | tee -a "${LOG_FP}" ;;
        'myFC' ) echo -e "${MESSAGE_PREFIX} myFC=${myFC}" 2>&1 | tee -a "${LOG_FP}" ;;
        'myFFLAGS' ) echo -e "${MESSAGE_PREFIX} myFFLAGS=${myFFLAGS}" 2>&1 | tee -a "${LOG_FP}" ;;
        'myFRFLAGS' ) echo -e "${MESSAGE_PREFIX} myFRFLAGS=${myFRFLAGS}" 2>&1 | tee -a "${LOG_FP}" ;;
        'myLINK_FLAG' ) echo -e "${MESSAGE_PREFIX} myLINK_FLAG=${myLINK_FLAG}" 2>&1 | tee -a "${LOG_FP}" ;;
        'NETCDF' ) echo -e "${MESSAGE_PREFIX} NETCDF=${NETCDF}" 2>&1 | tee -a "${LOG_FP}" ;;
        'NETCDF_DIR' ) echo -e "${MESSAGE_PREFIX} NETCDF_DIR=${NETCDF_DIR}" 2>&1 | tee -a "${LOG_FP}" ;;
        'PARMOD' ) echo -e "${MESSAGE_PREFIX} PARMOD=${PARMOD}" 2>&1 | tee -a "${LOG_FP}" ;;
        'STENEX' ) echo -e "${MESSAGE_PREFIX} STENEX=${STENEX}" 2>&1 | tee -a "${LOG_FP}" ;;
      esac # case "${KEY}"
      #   end debugging
    fi # [[ -r "${CONFIG_CMAQ_PATH}" ]]

  fi # [[ -z "${M3LIB}" ||

  if   [[ ! -d "${M3HOME}" ]] ; then
    echo -e "${ERROR_PREFIX} cannot read M3HOME='${M3HOME}', exiting ..." 2>&1 | tee -a "${LOG_FP}"
    exit 2
  elif [[ ! -d "${M3LIB}" ]] ; then
    echo -e "${ERROR_PREFIX} cannot read M3LIB='${M3LIB}', exiting ..." 2>&1 | tee -a "${LOG_FP}"
    exit 2
  elif [[ ! -d "${M3DATA}" ]] ; then
    echo -e "${ERROR_PREFIX} cannot read M3DATA='${M3DATA}', exiting ..." 2>&1 | tee -a "${LOG_FP}"
    exit 2
  fi

  ### build dir--not THIS_DIR, but the space in which we build
  # how to discover the target executable? more below, following were recovered *after* build
  ICON_EXEC_FN="${ICON_EXEC_PREFIX}_${EXEC_ID}"
  export MODEL="${ICON_EXEC_FN}" # Makefile wants this name
  ICON_BUILD_DIR_NAME="BLD_${ICON_EXEC_PREFIX}"
  ICON_BUILD_DIR_PATH="${M3HOME}/${ICON_BUILD_DIR_NAME}"
  ICON_EXEC_FP="${ICON_BUILD_DIR_PATH}/${ICON_EXEC_FN}"

  ### just a synonym for M3DATA
  INPUT_ROOT="${M3DATA}"

} # function setup_ICON_common_vars
